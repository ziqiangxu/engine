﻿/* Distributed under the Apache License, Version 2.0.
   See accompanying NOTICE file for details.*/

namespace HowToDriver
{
  class Program
  {
    static void Main(string[] args)
    {
      HowTo_UseEngine.Example.Run();
      //HowTo_PatientStates.Example.Run();
      //HowTo_Environment.Example.Run();
      //HowTo_MechanicalVentilator.Example.Run();
    }
  }
}
