if(NOT Pulse_PYTHON_API)
  return()
endif()
find_package(pybind11 CONFIG REQUIRED)

pybind11_add_module(PyPulse
  pybind/PyPulse.cpp
  pybind/PhysiologyEngine.cpp
  pybind/MultiplexVentilation.cpp)

target_include_directories(PyPulse PRIVATE ${pybind11_INCLUDE_DIRS})

if(APPLE)
    set_target_properties(PyPulse PROPERTIES MACOSX_RPATH ON)
endif()

target_link_libraries(PyPulse
                      PUBLIC MultiplexVentilationEngine pybind11::module
                      PRIVATE protobuf::libprotobuf
)

set_target_properties(PyPulse PROPERTIES
    DEBUG_POSTFIX "${PULSE_DEBUG_POSTFIX}"
    RELWITHDEBINFO_POSTFIX "${PULSE_RELWITHDEBINFO_POSTFIX}")

add_custom_command(TARGET PyPulse POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:PyPulse> ${CMAKE_INSTALL_PREFIX}/bin)

install(TARGETS PyPulse
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)

set_target_properties (PyPulse PROPERTIES FOLDER ${PROJECT_NAME})
