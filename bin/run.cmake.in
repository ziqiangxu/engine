# Scripts to assist developers
# Mostly these are calling the test suite driver and validator classes in Java


find_package(Java REQUIRED)
include(UseJava)
if(_JAVA_HOME)
  # Overwrite Java if the env variable is set
  
  message(STATUS "Using JAVA_HOME as my Java executable (set to : ${_JAVA_HOME})")
  set(Java_JAVA_EXECUTABLE ${_JAVA_HOME}/bin/java)
endif()
#  You tell me what JDK/JRE you want to use
#set(Java_JAVA_EXECUTABLE "/the_jdk_I_want/bin/java)

set(JAVA_CLASSPATH Pulse.jar
        "@CMAKE_SOURCE_DIR@/src/java/jar/commons-collections4-4.4.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/commons-compress-1.19.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/commons-math3-3.6.1.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/error_prone_annotations-2.3.4.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/gson-2.8.6.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/guava-29.0-jre.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/jfreechart-1.5.0.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/javassist.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/poi-4.1.2.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/poi-ooxml-4.1.2.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/poi-ooxml-schemas-4.1.2.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/reflections-0.9.12.jar"
        "@CMAKE_SOURCE_DIR@/src/java/jar/xmlbeans-3.1.0.jar")

if(NOT WIN32)
  string(REPLACE ";" ":" JAVA_CLASSPATH "${JAVA_CLASSPATH}")
endif()

if(TYPE STREQUAL "tests" OR TYPE STREQUAL "SystemValidation" OR TYPE STREQUAL "PatientValidation")
  if(TYPE STREQUAL "tests")
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver CDMUnitTests.config)
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver EngineUnitTests.config)
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver ScenarioVerification.config)
  endif()
  if(TYPE STREQUAL "SystemValidation" OR TYPE STREQUAL "tests")
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver SystemVerification.config)
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.validation.SystemValidation TEST)
  endif()
  if(TYPE STREQUAL "PatientValidation" OR TYPE STREQUAL "tests")
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver PatientVerification.config)
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.validation.PatientValidation TEST)
  endif()
elseif(TYPE STREQUAL "genData")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.dataset.DataSetReader)
elseif(TYPE STREQUAL "genStates")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver InitialPatientStates.config)
elseif(TYPE STREQUAL "doxygen")
  file(READ @CMAKE_SOURCE_DIR@/docs/Doxygen/header.html HEADER)
  file(READ @CMAKE_SOURCE_DIR@/docs/Doxygen/footer.html FOOTER)
  function(setup_landing_page CONTENT_FILE)
    file(READ @CMAKE_SOURCE_DIR@/docs/Doxygen/${CONTENT_FILE} CONTENT)
    file(WRITE  ./docs/html/${CONTENT_FILE} "${HEADER}")
    file(APPEND ./docs/html/${CONTENT_FILE} "${CONTENT}")
    file(APPEND ./docs/html/${CONTENT_FILE} "${FOOTER}")
  endfunction()
  file(MAKE_DIRECTORY  "./docs/html")
  file(MAKE_DIRECTORY  "./docs/markdown")
  file(GLOB_RECURSE DOCS "./docs/html/*.*" "./docs/markdown/*.*")
  if(DOCS)
    file(REMOVE ${DOCS})
  endif()
  file(COPY "@CMAKE_SOURCE_DIR@/docs/Doxygen/bootstrap" DESTINATION ./docs/html)
  file(COPY "@CMAKE_SOURCE_DIR@/docs/Doxygen/font-awesome-4.3.0" DESTINATION ./docs/html)
  file(COPY "@CMAKE_SOURCE_DIR@/docs/Images" DESTINATION ./docs/html)
  # Create the header and footer with panels for doxygen to use
  file(READ @CMAKE_SOURCE_DIR@/docs/Doxygen/header_panel.html HEADER_PANEL)
  file(READ @CMAKE_SOURCE_DIR@/docs/Doxygen/footer_panel.html FOOTER_PANEL)
  file(WRITE  @CMAKE_SOURCE_DIR@/docs/Doxygen/panel_header.html "${HEADER}")
  file(APPEND @CMAKE_SOURCE_DIR@/docs/Doxygen/panel_header.html "${HEADER_PANEL}")
  file(WRITE  @CMAKE_SOURCE_DIR@/docs/Doxygen/panel_footer.html "${FOOTER_PANEL}")
  file(APPEND @CMAKE_SOURCE_DIR@/docs/Doxygen/panel_footer.html "${FOOTER}")
  # These are run above
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.validation.SystemValidation BASELINE)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.validation.PatientValidation BASELINE)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.doxygen.DoxygenPreprocessor "@CMAKE_SOURCE_DIR@/docs/Markdown" ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.doxygen.DoxygenPreprocessor "@CMAKE_SOURCE_DIR@/docs/Methodology" ./docs/markdown ./test_results/tables)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.doxygen.Proto2Doxygen "@CMAKE_SOURCE_DIR@/src/schema" ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.doxygen.CDM2MD ./docs/markdown)
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.utilities.csv.plots.PlotDriver PlotRun.config)
  execute_process(COMMAND doxygen ./docs/full.doxy)
  # Build our landing files
  setup_landing_page(index.html)
  setup_landing_page(pulse-in-action.html)
  setup_landing_page(news.html)
elseif(TYPE STREQUAL "protoc")
  file(REMOVE @CMAKE_SOURCE_DIR@/src/schema/schema_last_built)
  execute_process(COMMAND ${CMAKE_COMMAND} -DSRC_ROOT:PATH=@CMAKE_SOURCE_DIR@/src
                                           -Dprotobuf_SRC:PATH=@protobuf_SRC@
                                           -DProtobuf_PROTOC_EXECUTABLE:PATH=@Protobuf_PROTOC_EXECUTABLE@
                                           -DPulse_JAVA_API:BOOL=@Pulse_JAVA_API@
                                           -DPulse_PYTHON_API:BOOL=@Pulse_PYTHON_API@
                                           -P "@CMAKE_SOURCE_DIR@/src/schema/GenerateBindings.cmake")
elseif(TYPE STREQUAL "updateBaselines")
  execute_process(COMMAND ${CMAKE_COMMAND} --build "@CMAKE_CURRENT_BINARY_DIR@" --target UpdateVerification)
elseif(TYPE STREQUAL "jar")
  execute_process(COMMAND ${CMAKE_COMMAND} --build "@CMAKE_CURRENT_BINARY_DIR@" --target PulseJava)
elseif(TYPE STREQUAL "rebase")
  # find Git and if available set GIT_HASH variable
  find_package(Git QUIET)
  if(GIT_FOUND)
    execute_process(
      COMMAND ${GIT_EXECUTABLE} log -1 --pretty=format:%h
      OUTPUT_VARIABLE GIT_HASH
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_QUIET
      WORKING_DIRECTORY
        @CMAKE_SOURCE_DIR@
      )
  endif()
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.Rebase ${GIT_HASH})
elseif(TYPE STREQUAL "FullReport")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SEReportResults)
elseif(TYPE STREQUAL "plotter")
  execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.utilities.csv.plots.CSVPlotTool ${ARG1})
else()
  if(${TYPE} MATCHES "Plot")
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.utilities.csv.plots.PlotDriver ${TYPE}.config)
  else()
    execute_process(COMMAND "${Java_JAVA_EXECUTABLE}" -classpath "${JAVA_CLASSPATH}" com.kitware.pulse.cdm.testing.SETestDriver ${TYPE}.config)
  endif()
endif()
